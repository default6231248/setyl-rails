# https://api.simplemdm.com/?_gl=1*1ziojq*_gcl_au*MTY2NTAyMTE2MC4xNzAxMjQyNzU4#introduction
# https://api.simplemdm.com/?_gl=1*1ziojq*_gcl_au*MTY2NTAyMTE2MC4xNzAxMjQyNzU4#authentication
# https://api.simplemdm.com/?_gl=1*1ziojq*_gcl_au*MTY2NTAyMTE2MC4xNzAxMjQyNzU4#pagination

class SimpleMdmClient
  class BadResponseError < RuntimeError
    attr_reader :body, :headers

    def initialize(response)
      @body = response.parsed_response
      @headers = response.headers
      super("Unexpected response code: #{response.code}; #{response.parsed_response}")
    end
  end

  include HTTParty

  def initialize(api_key:)
    @api_key = api_key
  end

  # https://api.simplemdm.com/?_gl=1*1ziojq*_gcl_au*MTY2NTAyMTE2MC4xNzAxMjQyNzU4#list-all-6
  def devices(
    limit: 10,
    starting_after: nil,
    include_awaiting_enrollment: false,
    include_secret_custom_attributes: false
  )
    options = {
      headers: { 'Content-Type' => 'application/json' },
      basic_auth: { username: @api_key },
      query: {
        include_awaiting_enrollment:,
        include_secret_custom_attributes:,
        limit:,
        starting_after:,
        direction: 'asc'
      },
      timeout: 5.minutes.in_seconds
    }

    response = self.class.get('https://a.simplemdm.com/api/v1/devices', options)

    # https://medium.com/@guillaume.viguierjust/rate-limiting-your-restful-api-3148f8e77248
    # The window could be negative because of travel time bewtween the server and client
    # if response.code == 429
    #   window = Time.at(response.headers['x-ratelimit-reset'].to_i) - Time.now
    #   return { 'rate_limit_window' => window }
    #   sleep(window.ceil) within SimpleMdmConnectionService.import_device_asset_sources!
    # end

    raise BadResponseError, response if response.code != 200
    response
  end
end
