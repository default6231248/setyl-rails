class AssetSourceMutator
  class << self
    def create_microsoft_managed_device!(company, device_info)
      device = company.microsoft_managed_devices.find_or_initialize_by(external_id: device_info['id'])
      device.microsoft_subscription = company.microsoft_subscription
      device.external_device_id = device_info['azureADDeviceId']
      device.external_user_id = device_info['userId']
      device.data = device_info
      device.save!

      asset_source = company.asset_sources.find_or_initialize_by(sourceable: device)
      asset_source.owner_type = device_info['managedDeviceOwnerType']
      asset_source.external_device_id = device_info['azureADDeviceId']
      asset_source.name = device_info['deviceName']
      asset_source.model = device_info['model']
      asset_source.manufacturer = device_info['manufacturer']
      asset_source.operating_system = device_info['operatingSystem']
      asset_source.serial_number = device_info['serialNumber']
      asset_source.imei = device_info['imei']
      asset_source.save!

      asset_source
    end

    def create_microsoft_user_owned_device!(company, user, device_info)
      device = company.microsoft_user_owned_devices.find_or_initialize_by(external_id: device_info['id'])
      device.user = user
      device.microsoft_subscription = company.microsoft_subscription
      device.external_device_id = device_info['deviceId']
      device.data = device_info
      device.save!

      asset_source = company.asset_sources.find_or_initialize_by(sourceable: device)
      asset_source.owner_type = device_info['deviceOwnership']
      asset_source.external_device_id = device_info['deviceId']
      asset_source.name = device_info['displayName']
      asset_source.model = device_info['model']
      asset_source.manufacturer = device_info['manufacturer']
      asset_source.operating_system = device_info['operatingSystem']
      asset_source.save!

      asset_source
    end

    def create_google_device!(company, user_info, device_info)
      device = company.google_devices.find_or_initialize_by(external_device_id: device_info.device_id)
      device.user = company.users.web.find_by(email: user_info.user_email.strip.downcase)
      device.external_user_id = user_info.name.split('/').last
      device.name = device_info.name
      device.last_sync_time = device_info.last_sync_time
      device.owner_type = device_info.owner_type
      device.model = device_info.model
      device.manufacturer = device_info.manufacturer
      device.os_version = device_info.os_version
      device.device_type = device_info.device_type
      device.serial_number = device_info.serial_number
      device.imei = device_info.imei
      device.manufacturer = device_info.manufacturer
      device.external_id = device_info.device_id
      device.encryption_status = device_info.encryption_state
      device.data = device_info.to_h
      device.save!

      asset_source = company.asset_sources.find_or_initialize_by(sourceable: device)
      # asset_source.name = "#{device.serial_number} #{device.user.name}"
      asset_source.name = device.external_device_id
      asset_source.model = device.model
      asset_source.manufacturer = device.manufacturer
      asset_source.operating_system = device.os_version
      asset_source.owner_type = device.owner_type_to_asset_source_owner_type
      asset_source.serial_number = device.serial_number
      asset_source.imei = device.imei
      asset_source.external_device_id = device.external_id
      asset_source.save!

      asset_source
    end

    def create_jamf_computer!(computer_preview)
      company = computer_preview.company
      detail = computer_preview.detail.presence || computer_preview.detail.none

      user = find_user_by_jamf_data(company, computer_preview.data)
      computer_preview.update!(user:) if user.present?

      asset_source = company.asset_sources.find_or_initialize_by(sourceable: computer_preview)
      asset_source.external_device_id = computer_preview.external_id
      asset_source.name = computer_preview.name || computer_preview.udid
      asset_source.model = detail.data.dig('hardware', 'model')
      asset_source.manufacturer = detail.data.dig('hardware', 'make')
      asset_source.operating_system = "#{detail.data.dig('operatingSystem', 'name')} #{detail.data.dig('operatingSystem', 'version')}"
      asset_source.serial_number = computer_preview.data['serialNumber']
      asset_source.asset_tag = computer_preview.data['assetTag']
      asset_source.specifics = computer_preview.data['modelIdentifier']
      asset_source.purchase_price = detail.data.dig('purchasing', 'purchasePrice')
      asset_source.purchase_ref = detail.data.dig('purchasing', 'poNumber')
      asset_source.warranty_date = Date.safe_parse(detail.data.dig('purchasing', 'warrantyDate'))
      asset_source.save!

      asset_source
    end

    def create_jamf_mobile_device!(mobile_device)
      company = mobile_device.company
      detail = mobile_device.detail.presence || mobile_device.detail.none

      user = find_user_by_jamf_data(company, detail.data)
      mobile_device.update!(user:) if user.present?

      asset_source = company.asset_sources.find_or_initialize_by(sourceable: mobile_device)
      asset_source.external_device_id = mobile_device.external_id
      asset_source.name = mobile_device.name || mobile_device.udid
      asset_source.model = mobile_device.data['model']
      asset_source.operating_system = "#{mobile_device.data['type']} #{detail.data['osVersion']}"
      asset_source.serial_number = mobile_device.data['serialNumber']
      asset_source.asset_tag = detail.data['assetTag']
      asset_source.specifics = mobile_device.data['modelIdentifier']
      asset_source.purchase_price = detail.data.dig('purchasing', 'purchasePrice')
      asset_source.purchase_ref = detail.data.dig('purchasing', 'poNumber')
      asset_source.warranty_date = Date.safe_parse(detail.data.dig('purchasing', 'warrantyExpiresDate'))
      asset_source.owner_type = detail.data['deviceOwnershipLevel']
      asset_source.save!

      asset_source
    end

    def create_okta_device!(device)
      company = device.company
      asset_source = company.asset_sources.find_or_initialize_by(sourceable: device)
      asset_source.external_device_id = device.external_id
      asset_source.name = device.name
      asset_source.manufacturer = device.data.dig('profile', 'manufacturer')
      asset_source.model = device.data.dig('profile', 'model')
      asset_source.operating_system = "#{device.data.dig('profile', 'platform')} #{detail.data.dig('profile', 'osVersion')}"
      asset_source.serial_number = device.data.dig('profile', 'serialNumber')
      asset_source.imei = device.data.dig('profile', 'imei')
      asset_source.save!

      asset_source
    end

    def create_simple_mdm_device!(device)
      # Why some mutators have both devices and asset sources, some asset sources only?
      # device.encryption_status = device_info.encryption_state

      # What is asset tag?
      # asset_source.asset_tag = 'xxx'

      company = device.company
      asset_source = company.asset_sources.find_or_initialize_by(sourceable: device)
      asset_source.external_device_id = device.external_id
      asset_source.name = device.name
      asset_source.manufacturer = 'Apple'
      asset_source.model = device.data.dig('attributes', 'model_name')

      operating_system = device.data.dig('attributes', 'os_version')
      operating_system = "iOS #{operating_system}" if asset_source.model.match?('iPhone')
      asset_source.operating_system = operating_system

      asset_source.serial_number = device.data.dig('attributes', 'serial_number')
      asset_source.specifics = device.data.dig('attributes', 'product_name')
      asset_source.imei = device.data.dig('attributes', 'imei')
      asset_source.save!

      asset_source
    end

    private

    def find_user_by_jamf_data(company, data)
      email = data.dig('location', 'emailAddress')
      return company.users.web.find_by(email: email.strip.downcase) if email.present?

      first_name, last_name = (data.dig('location', 'username') || '').split
      return company.users.web.find_by(first_name:, last_name:) if first_name.present? && last_name.present?

      nil
    end
  end
end
