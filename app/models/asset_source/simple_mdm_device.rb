class AssetSource::SimpleMdmDevice < ApplicationRecord
  belongs_to :company
  belongs_to :simple_mdm_connection
  belongs_to :user, optional: true

  validates :external_id, presence: true
  # validates :udid, presence: true
  validates :simple_mdm_connection, uniqueness: { scope: :external_id }

  def provider_name
    'SimpleMDM'
  end

  def provider_slug
    'simple_mdm'
  end

  def source_name
    'Device'
  end

  def raw_data
    data
  end

  # How do we use it?
  def encryption_status
    nil
  end

  def simple_mdm?
    true
  end

  def jamf?
    false
  end

  def microsoft?
    false
  end

  def google?
    false
  end

  def okta?
    false
  end
end
