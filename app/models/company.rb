class Company < ApplicationRecord
  has_many :asset_sources

  scope :not_removed, -> { where(removed_at: nil) }
end
