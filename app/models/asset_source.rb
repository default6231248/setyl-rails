class AssetSource < ApplicationRecord
  extend Enumerize

  belongs_to :sourceable, polymorphic: true
  belongs_to :company
  belongs_to :asset, class_name: 'Device', optional: true

  validates :name, :external_device_id, presence: true

  delegate :provider_name, :provider_slug, :source_name, :raw_data, :microsoft?, :google?, :jamf?, :okta?, :simple_mdm?, to: :sourceable
  delegate :user, to: :sourceable, allow_nil: true

  RANSACK_SEARCH_FIELDS = %w[
    name
    manufacturer
    model
    operating_system
    owner_type
    serial_number
    imei
    sourceable_of_AssetSource::GoogleDevice_type_user_full_name
    sourceable_of_AssetSource::JamfComputerPreview_type_user_full_name
    sourceable_of_AssetSource::JamfMobileDevice_type_user_full_name
    sourceable_of_AssetSource::MicrosoftUserOwnedDevice_type_user_full_name
    sourceable_of_AssetSource::OktaDevice_type_user_full_name
    # sourceable_of_AssetSource::SimpleMdmDevice_type_user_full_name
  ].freeze
  search_predicate = RANSACK_SEARCH_FIELDS.join('_or_').to_sym
  ransack_alias :asset_source_fields, search_predicate

  state_machine initial: :initial do
    state :initial
    state :linked
    state :ignored

    event :mark_as_linked do
      transition all => :linked
    end

    event :ignore do
      transition all => :ignored
    end

    event :mark_as_initial do
      transition all => :initial
    end
  end

  TIMELINE_EVENT_ATTRIBUTES = %i[name model manufacturer operating_system owner_type serial_number
                                 imei external_device_id state].freeze
  TIMELINE_EVENT_RELATIONS = {
    asset: %i[uuid manufacturer model],
    user: %i[uuid first_name last_name]
  }.freeze

  include AssetSourceRepository
  # include AssetSourcePresenter

  def initialize(attrs = {})
    super({ manually_unlinked: false, auto_linked: false }.merge(attrs || {}))
  end
end
