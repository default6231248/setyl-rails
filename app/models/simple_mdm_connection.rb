class SimpleMdmConnection < ApplicationRecord
  belongs_to :company
  belongs_to :creator, class_name: 'User'

  validates :api_key, presence: true
  validates :company, uniqueness: { conditions: -> { connected } }

  state_machine initial: :initial do
    state :initial
    state :connected
    state :disconnected
    state :failed

    event :connect do
      transition all => :connected
    end

    event :disconnect do
      transition all => :disconnected
    end

    event :mark_as_failed do
      transition all => :failed
    end

    after_transition on: :connect, do: :mark_integration_as_connected
    after_transition on: :disconnect, do: :mark_integration_as_disconnected
    after_transition on: :mark_as_failed, do: :mark_integration_as_failed
  end

  state_machine :sync_state, initial: :not_running, namespace: :sync do
    state :not_running
    state :running

    event :start do
      transition not_running: :running
    end

    event :stop do
      transition all => :not_running
    end

    before_transition on: :start, do: :set_sync_started_date
    after_transition on: :start, do: :mark_integration_as_running
    after_transition on: :stop, do: :mark_integration_as_not_running
  end

  include SimpleMdmConnectionRepository

  def integration_slug
    :simple_mdm
  end

  private

  def set_sync_started_date
    self.sync_started_at = Time.current
  end

  def mark_integration_as_connected
    Integration.ready.except(:order).simple_mdm_connection.find_each do |integration|
      company_integration = integration.company_integrations.find_or_initialize_by(company:)
      company_integration.slug = integration.slug
      company_integration.connection = self
      company_integration.connect!
    end
  end

  def mark_integration_as_disconnected
    Company::Integration.where(connection: self).find_each(&:disconnect!)
  end

  def mark_integration_as_failed
    Company::Integration.where(connection: self).find_each(&:mark_as_failed!)
  end

  def mark_integration_as_running
    # Company::Integration.web.sync_not_running.where(connection: self).find_each(&:start_sync!)
  end

  def mark_integration_as_not_running
    # Company::Integration.sync_running.where(connection: self).find_each(&:stop_sync!)
  end
end
