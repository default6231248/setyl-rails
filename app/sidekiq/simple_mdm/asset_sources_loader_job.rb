class SimpleMdm::AssetSourcesLoaderJob
  include Sidekiq::Job

  sidekiq_options retry: 0

  def perform(connection_id)
    connection = SimpleMdmConnection.without_removed_company.connected.find_by(id: connection_id)
    return if connection.blank?

    three_hours_ago = Time.now.utc - 3.hours
    return if connection.sync_running? && (connection.sync_started_at > three_hours_ago)

    connection.stop_sync! if connection.sync_running?
    connection.start_sync!
    # send_broadcast(connection)

    SimpleMdmConnectionService.import_device_asset_sources!(connection)

    connection.stop_sync!
    # send_broadcast(connection)
  rescue StandardError => e
    if connection.present?
      connection.stop_sync!
      # send_broadcast(connection)
    end

    raise e
  end

  private

  def send_broadcast(connection)
    sleep 2
    Company::Integration.web.where(connection:).find_each do |integration|
      partial = ApplicationController.render(partial: 'web/integrations/integration_card', locals: { integration: })
      ActionCable.server.broadcast("company_integration_#{integration.id}", { partial: })
    end
  end
end
