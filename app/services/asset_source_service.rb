class AssetSourceService
  # extend TimelineEventTracker

  class << self
    def create_microsoft_managed_device!(company, device_info)
      asset_source = AssetSourceMutator.create_microsoft_managed_device!(company, device_info)
      AssetSourceService.auto_link!(asset_source)
    end

    def create_microsoft_user_owned_device!(company, user, device_info)
      asset_source = AssetSourceMutator.create_microsoft_user_owned_device!(company, user, device_info)
      AssetSourceService.auto_link!(asset_source)
    end

    def create_google_device!(company, google_user, google_device)
      asset_source = AssetSourceMutator.create_google_device!(company, google_user, google_device)
      AssetSourceService.auto_link!(asset_source)
    end

    def create_jamf_computer!(computer_preview)
      asset_source = AssetSourceMutator.create_jamf_computer!(computer_preview)
      AssetSourceService.auto_link!(asset_source)
    end

    def create_jamf_mobile_device!(mobile_device)
      asset_source = AssetSourceMutator.create_jamf_mobile_device!(mobile_device)
      AssetSourceService.auto_link!(asset_source)
    end

    def create_okta_device!(device)
      asset_source = AssetSourceMutator.create_okta_device!(device)
      AssetSourceService.auto_link!(asset_source)
    end

    def create_simple_mdm_device!(device)
      asset_source = AssetSourceMutator.create_simple_mdm_device!(device)
      # AssetSourceService.auto_link!(asset_source)
    end

    def auto_link!(asset_source)
      company = asset_source.company
      return if !asset_source.initial?
      return if asset_source.manually_unlinked?

      serial_number = asset_source.serial_number
      return if serial_number.blank?
      return if serial_number.strip.casecmp('0').zero?
      return if serial_number.strip.casecmp('unknown').zero?
      return if company.assets.web.where(serial_number:).count != 1

      # What are company web assets?
      asset = company.assets.web.find_by(serial_number:)
      return if asset.blank?

      snapshot = snapshot_associations_for_track(asset_source)

      asset_source.asset = asset
      asset_source.auto_linked = true
      ApplicationRecord.transaction do
        asset_source.save!
        asset_source.mark_as_linked!
        track!(:mdm_auto_link_to_asset, target: asset_source, related_target: asset, company:).stamp!(asset_source, snapshot)
      end
    end
  end
end
