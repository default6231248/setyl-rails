class SimpleMdmConnectionService
  def self.import_device_asset_sources!(connection)
    client = SimpleMdmClient.new(api_key: connection.api_key)
    starting_after = nil

    loop do
      # devices = client.devices(limit: 100, starting_after:)
      devices = client.devices(limit: 100, starting_after:, include_awaiting_enrollment: true)

      devices['data'].each do |device|
        simple_mdm_device = AssetSource::SimpleMdmDevice.find_or_initialize_by(
          simple_mdm_connection: connection,
          external_id: device['id']
        )

        simple_mdm_device.company = connection.company
        simple_mdm_device.udid = device.dig('attributes', 'unique_identifier')
        simple_mdm_device.name = device.dig('attributes', 'name')
        # simple_mdm_device.name = device.dig('attributes', 'device_name')
        simple_mdm_device.data = device
        simple_mdm_device.save!

        AssetSourceService.create_simple_mdm_device!(simple_mdm_device)
      end

      break if devices['has_more'] == false
      starting_after = devices['data'][-1]['id']
    end
  end
end
