module AssetSourceRepository
  extend ActiveSupport::Concern
  include StateMachineScopes

  included do
    scope :web, -> { not_ignored }
    scope :microsoft, -> { where(sourceable_type: %i[AssetSource::MicrosoftManagedDevice AssetSource::MicrosoftUserOwnedDevice]) }
    scope :google, -> { where(sourceable_type: %i[AssetSource::GoogleDevice]) }
    scope :jamf, -> { where(sourceable_type: %i[AssetSource::JamfComputerPreview AssetSource::JamfMobileDevice]) }
    scope :okta, -> { where(sourceable_type: %i[AssetSource::OktaDevice]) }
    scope :simple_mdm, -> { where(sourceable_type: %i[AssetSource::SimpleMdmDevice]) }

    scope :sort_by_asset_name_asc, -> { left_joins(:asset).order('LOWER(devices.manufacturer) asc, LOWER(devices.model) asc') }
    scope :sort_by_asset_name_desc, -> { left_joins(:asset).order('LOWER(devices.manufacturer) desc, LOWER(devices.model) desc') }

    scope :microsoft_distinct_by_external_device_id, -> { microsoft.select('DISTINCT ON (external_device_id) *') }
    scope :google_distinct_by_serial_number, -> { google.select('DISTINCT ON (serial_number) *') }
    scope :deduplicated, -> {
      from(
        Arel::Nodes::As.new(
          Arel::Nodes::UnionAll.new(
            Arel::Nodes::UnionAll.new(
              Arel::Nodes::UnionAll.new(
                microsoft_distinct_by_external_device_id.arel,
                google_distinct_by_serial_number.arel
              ), jamf.arel
            ), okta.arel
          ), arel_table
        )
      )
    }

    scope :available_to_auto_link, -> { initial.where(manually_unlinked: false) }
  end
end
