module StateMachineScopes
  extend ActiveSupport::Concern

  included do
    state_machines.each do |name, sm|
      sm.states.each do |s|
        scope_name = sm.namespace.present? ? :"#{sm.namespace}_#{s.name}" : s.name
        scope scope_name, -> { where(name => s.name) }

        inversed_scope_name = sm.namespace.present? ? :"#{sm.namespace}_not_#{s.name}" : "not_#{s.name}"
        scope inversed_scope_name, -> { where.not(name => s.name) }
      end
    end
  end
end
