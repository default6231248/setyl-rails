module SimpleMdmConnectionRepository
  extend ActiveSupport::Concern
  include StateMachineScopes

  included do
    # scope :web, -> { with_state(:connected, :failed) }
    scope :without_removed_company, -> { joins(:company).merge(Company.not_removed) }
  end
end
