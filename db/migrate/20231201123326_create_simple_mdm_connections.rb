class CreateSimpleMdmConnections < ActiveRecord::Migration[7.1]
  def change
    create_table :simple_mdm_connections do |t|
      # https://rbbrio.slack.com/archives/C01K8JRN19U/p1701773290047389?thread_ts=1701773076.611689&cid=C01K8JRN19U
      # Use t.references
      t.bigint "creator_id", null: false
      t.bigint "company_id", null: false
      t.string "username", null: false
      t.string "state"
      t.string "sync_state"
      t.datetime "sync_started_at"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["company_id"], name: "index_simple_mdm_connections_on_company_id"
      t.index ["creator_id"], name: "index_simple_mdm_connections_on_creator_id"
    end
  end
end
