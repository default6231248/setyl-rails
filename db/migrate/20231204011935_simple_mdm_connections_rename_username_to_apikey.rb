class SimpleMdmConnectionsRenameUsernameToApikey < ActiveRecord::Migration[7.1]
  def change
    rename_column :simple_mdm_connections, :username, :api_key
  end
end
