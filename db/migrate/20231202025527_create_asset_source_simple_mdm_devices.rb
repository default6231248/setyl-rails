class CreateAssetSourceSimpleMdmDevices < ActiveRecord::Migration[7.1]
  def change
    create_table :asset_source_simple_mdm_devices do |t|
      t.bigint "company_id", null: false
      t.bigint "simple_mdm_connection_id", null: false
      t.bigint "user_id"
      t.string "external_id"
      t.string "udid"
      t.string "name"
      t.jsonb "data"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["company_id"], name: "index_asset_source_simple_mdm_devices_on_company_id"
      t.index ["simple_mdm_connection_id", "external_id"], name: "index_simple_mdm_devices_on_connection_id_and_external_id", unique: true
      t.index ["simple_mdm_connection_id"], name: "index_simple_mdm_devices_on_connection_id"
      t.index ["user_id"], name: "index_asset_source_simple_mdm_devices_on_user_id"
    end
  end
end
