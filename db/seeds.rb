# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

# https://rbbrio.slack.com/archives/C01K8JRN19U/p1701773220560659?thread_ts=1701773076.611689&cid=C01K8JRN19U
# Use fixtures

company = Company.find_or_create_by!(
  name: 'setyl',
  removed_at: nil,
  currency: 'xxx',
  plan_state: 'xxx'
)

user = User.find_or_create_by!(
  company:,
  first_name: 'Maxim'
)

SimpleMdmConnection.find_or_create_by!(
  company:,
  creator: user,
  api_key: ENV.fetch('SIMPLE_MDM_API_KEY'),
  state: 'connected',
  sync_state: 'not_running'
)
