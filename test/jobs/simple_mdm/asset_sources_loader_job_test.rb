require 'test_helper'

class SimpleMdm::AssetSourcesLoaderJobTest < ActiveSupport::TestCase
  setup do
    @connection = simple_mdm_connections(:setyl)
    @device_external_id = 121
  end

  test '#perform' do
    devices = file_fixture('simple_mdm/devices.json')

    stub_request(:get, 'https://a.simplemdm.com/api/v1/devices?direction=asc&include_awaiting_enrollment=false&include_secret_custom_attributes=false&limit=100&starting_after=')
      .with(headers: { 'Accept' => '*/*',
                       'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                       'Authorization' => 'Basic ZVBTWVJjdENrc2JKYW1xaG5BeHh4Og==',
                       'Content-Type' => 'application/json',
                       'User-Agent' => 'Ruby' })
      .to_return(status: 200, body: devices, headers: { 'Content-Type' => 'application/json' })

    SimpleMdm::AssetSourcesLoaderJob.new.perform(@connection.id)

    device = AssetSource.simple_mdm.find_by(external_device_id: @device_external_id)

    assert { device.present? }
    assert { device.sourceable.present? }
    assert { device.name == "Mike's iPhone" }
    assert { device.model == 'iPhone 7' }
    assert { device.manufacturer == 'Apple' }
    assert { device.operating_system == 'iOS 14.7' }
    assert { device.serial_number == 'DNFJE9DNG5MG' }
    assert { device.specifics == 'iPhone9,1' }
    assert { device.imei == '35 445506 652132 5' }
  end
end
